package gui;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import algorithmen.Generator;
import algorithmen.MinoVerwalter;
import element.Mino;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import manager.Laden;
import manager.Speichern;
import javafx.stage.FileChooser.ExtensionFilter;

/** Dies ist das Hauptfenster dieser Anwendung */
public class Fenster extends Stage implements EventHandler<ActionEvent>{
	
	private MinoVerwalter mv;
	private Bild bild;
	private BorderPane layout;
	private MenuBar menubar;
	private Menu menu, rmenu, smenu;
	private MenuItem[] menuitemfeld;
	private boolean farbe, zwischenstand;
	
	/** Erzeugt das Hauptfenster
	 * 
	 * @param b breite des Feldes fuer den Generator aus Hauptmenu
	 * @param h hoehe des Feldes fuer den Generator aus Hauptmenu
	 */
	Fenster(int b, int h){
		farbe = true;
		zwischenstand = false;
		
		Generator g = new Generator(b,h);
		
		/* bricht ab, wenn ein gueltiges Ruetsel generiert wurde */
		while ( g.getOK() ){
			g = new Generator(b,h);
		}
		
		mv = new MinoVerwalter(this);
		mv.setMinoDaten(b,h,g.getRaetsel(),g.getLoesung());
		initKomponenten();
		initLayout();
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setTitle("Fillomino");
		setResizable(false);
		show();
	}
	
	/** Erzeugt das Hauptfenster, nachdem ein Spielstand geladen wurde
	 * 
	 * @param groesse die Raetselgroesse
	 * @param aktuell das ungeloeste Raetsel
	 * @param loesung die Loesung des Raetsels
	 */
	public Fenster(int[] groesse, Mino[][] aktuell, Mino[][] loesung){
		farbe = true;
		zwischenstand = false;
		mv = new MinoVerwalter(this);
		mv.setMinoDaten(groesse[0], groesse[1],aktuell,loesung);
		initKomponenten();
		initLayout();
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setTitle("Fillomino");
		setResizable(false);
		show();
	}
	
	/** initialisiert die Komponenten */
	private void initKomponenten(){
		menubar = new MenuBar();
		menu = new Menu("Spiel");
		rmenu = new Menu("RÃ¤tsel");
		smenu = new Menu("Sonstiges");
		menuitemfeld = new MenuItem[10];
		menuitemfeld[0] = new MenuItem("Neues Spiel");
		menuitemfeld[1] = new MenuItem("Neu versuchen");
		menuitemfeld[2] = new MenuItem("LÃ¶sung zeigen");
		menuitemfeld[3] = new MenuItem("PrÃ¼fe");
		menuitemfeld[4] = new MenuItem("Farbe/SW");
		menuitemfeld[5] = new MenuItem("Grafik speichern");
		menuitemfeld[6] = new MenuItem("Spiel Speichern");
		menuitemfeld[7] = new MenuItem("Spiel Laden");
		menuitemfeld[8] = new MenuItem("Regeln");
		menuitemfeld[9] = new MenuItem("Beenden");
		for(int i = 0; i < menuitemfeld.length; i++){
			menuitemfeld[i].setOnAction(this);
		}
		menu.getItems().addAll(menuitemfeld[0], menuitemfeld[1], menuitemfeld[8], menuitemfeld[9]);
		rmenu.getItems().addAll(menuitemfeld[3], menuitemfeld[2]);
		smenu.getItems().addAll(menuitemfeld[4], menuitemfeld[5], menuitemfeld[6], menuitemfeld[7]);
		menubar.getMenus().addAll(menu, rmenu, smenu);	
		bild = new Bild(this);
	}
	
	/** initialisiert das Layout */
	private void initLayout(){
		layout = new BorderPane();
		layout.setTop(menubar);
		layout.setCenter(bild);
	}
	
	/** veranlasst das generieren eines neuen Raetsel */
	private void neu(){
		new Hauptmenu();
		close();
	}

	/** zeigt die Loesung des Raetsels in ein neuen Fenster an */
	private void loesung(){
		new LFenster(mv.getLoesung());
	}
	
	/** regelt, ob das Raetsel farbig sein soll oder schwarz/weiss */
	private void farbwechsel(){
		if ( farbe == true ){
			bild.zeichnen(false);
			menuitemfeld[3].setDisable(true);
			farbe = false;
		} else {
			bild.zeichnen(true);
			farbe = true;
			menuitemfeld[3].setDisable(false);
		}
	}
	
	/** regelt, ob der zwischenstand des Raetsel als Gruen/Rot angezeigt werden soll
	 * 
	 * @return true, zwischenstand wird gezeichnet, false zwischenstand wird nicht gezeichnet
	 */
	public boolean zwischenstandWechsel(){
		if ( zwischenstand == true ){
			zwischenstand = false;
			menuitemfeld[1].setDisable(false);
			smenu.setDisable(false);
			return zwischenstand;
		} else {
			zwischenstand = true;
			menuitemfeld[1].setDisable(true);
			smenu.setDisable(true);
		}
		return zwischenstand;
	}
	
	/** zeigt die Spielregeln an */
	public void spielregeln(){
		new Regel();
	}
	
	/**
	 * speichert ein generiertes ungeloestes Raetsel als png-Grafik
	 */
	private void speicherGrafik(){
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("PNG-Datei", "*.png"));
		File f = fc.showSaveDialog(this);
		
		if ( f != null ){
			try {
				WritableImage img = new WritableImage((int) bild.getWidth(), (int) bild.getHeight());
				bild.snapshot(null, img);
				RenderedImage rimg = SwingFXUtils.fromFXImage(img, null);
				ImageIO.write(rimg, "png", f);
				Alert a = new Alert(AlertType.INFORMATION);
				a.setHeaderText("PNG-Grafik gespeichert");
				a.setContentText("Grafik wurde gespeichert");
				a.showAndWait();
			} catch ( IOException ioe ){
				Alert a = new Alert(AlertType.WARNING);
				a.setHeaderText("Fehler");
				a.setContentText("Grafik konnte nicht gespeichert werden");
				a.showAndWait();
			}
		}
	}
	
	/** speichert den aktuellen Fortschritt ab */
	private void speichern(){
		new Speichern(mv.getRaetselgroesse(), mv.getMinoFeld(), mv.getLoesung());
	}
	
	private void laden(){
		new Laden(this);
	}

	/** ueberschriebene Methode aus EventHandler */
	@Override
	public void handle(ActionEvent action) {
		if ( action.getSource() == menuitemfeld[0] ){
			neu();
		} else if ( action.getSource() == menuitemfeld[1] ){
			mv.zuruecksetzen();
		} else if ( action.getSource() == menuitemfeld[2] ){
			loesung();
		} else if ( action.getSource() == menuitemfeld[3] ){
			mv.zwischenergebnis();
		} else if ( action.getSource() == menuitemfeld[4] ){
			farbwechsel();
		} else if ( action.getSource() == menuitemfeld[5] ){
			speicherGrafik();
		} else if ( action.getSource() == menuitemfeld[6] ){
			speichern();
		} else if ( action.getSource() == menuitemfeld[7] ){
			laden();
		} else if ( action.getSource() == menuitemfeld[8] ){
			spielregeln();
		} else if ( action.getSource() == menuitemfeld[9] ){
			System.exit(0);
		}
	}

	/**
	 * setzt die beiden Minomatrizen nach den Laden eines Spielstandes
	 * @param groesse die groesse des Raetsels
	 * @param aktuell aktuelles Raetsel
	 * @param loesung geloestes Raetsel
	 */
	public void setMinoFelder(int[] groesse, Mino[][] aktuell, Mino[][] loesung){
		close();
		new Fenster(groesse,aktuell,loesung);
	}
	
	/**
	 * gibt den booleschen Wert der Varibale zurueck, ob der Zwischenstand
	 * angezeigt werden soll oder nicht (Gruen = richtig, Rot = falsch)
	 * @return zwischenstand anzeigen
	 */
	public boolean getZwischenstand(){
		return zwischenstand;
	}
	
	/**
	 * gibt den booleschen Wert der Varibale zurueck, ob das Raetsel in
	 * Farben gezeichnet werden soll oder nur schwarz/weiss
	 * @return farbe
	 */
	public boolean getFarbe(){
		return farbe;
	}
	
	/** gibt die Bildreferrenz zurueck 
	 * 
	 * @return bildreferrenz
	 */
	public Bild getBild(){
		return bild;
	}
	
	/** gibt die Minoverwalterreferrenz zurueck
	 * 
	 * @return minoverwalterreferenz
	 */
	MinoVerwalter getMinoVerwalter(){
		return mv;
	}
}

package gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/** Diese Klasse dient als Benutzereingabe fuer die Minofeldbelegungen */
public class Zahleneingabe extends Stage implements EventHandler<ActionEvent>{

	private BorderPane layout;
	private Button[][] auswahl;
	private int zurueck;
	
	/**
	 * Konstruktor erzeugt die Fensterdarstellung
	 */
	public Zahleneingabe(){
		initModality(Modality.APPLICATION_MODAL);
		initialisieren();	
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setResizable(false);
		showAndWait();
	}
	
	/** initialisiert die Komponenten */
	private void initialisieren(){
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(5,5,5,5));
		auswahl = new Button[3][3];
		int zaehler = 2;
		for(int x = 0; x < 3; x++){
			for(int y = 0; y < 3; y++){
				auswahl[y][x] = new Button(""+zaehler);
				auswahl[y][x].setOnAction(this);
				auswahl[y][x].setMinSize(40, 40);
				Pane pane = new Pane();
				pane.setPadding(new Insets(1,1,1,1));
				pane.getChildren().add(auswahl[y][x]);
				grid.add(pane, y, x);			
				zaehler++;
			}
		}	
		layout = new BorderPane();
		layout.setTop(new Label("Minozahl eingeben"));
		layout.setCenter(grid);
	}
	
	/**
	 * veranlasst Aktionen am Raetsel, je nachdem welcher Wert
	 * ausgewaehlt wurde.
	 * wert : 2 - 10 : Minoblock erzeugen
	 * @param wert zahlenfeldeingabe
	 */
	private void interpretiere(int wert){
		if ( wert >= 2 & wert <= 10){
			zurueck = wert;
		}
		close();
	}
	
	/** ueberschriebene Methode aus EventHandler */
	@Override
	public void handle(ActionEvent action) {
		int zaehler = 1;
		for(int x = 0; x < 3; x++){
			for(int y = 0; y < 3; y++){
				zaehler++;
				if ( action.getSource() == auswahl[y][x] ){
					interpretiere(zaehler);
					return;
				}
			}
		}
	}
	
	/**
	 * gibt den in das Zahlenfeld gewaehlten Wert zurueck
	 * @return zahleneingabewert
	 */
	public int getEingabewert(){
		return zurueck;
	}

}

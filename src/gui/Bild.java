package gui;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * Erzeugt eine Canvas um den Inhalt des Spiels grafisch darzustellen
 */
public class Bild extends Canvas implements EventHandler<MouseEvent> {
	
	private Fenster fenster;
	private GraphicsContext gc;
	private final int ZELLE = 32;
	
	/**
	 * erzeugt eine Canvas worauf der Inhalt gezeichnet wird
	 * @param f fensterreferrenz
	 */
	Bild(Fenster f){
		fenster = f;
		gc = getGraphicsContext2D();
		setOnMouseClicked(this);
		setWidth(ZELLE*fenster.getMinoVerwalter().getRaetselgroesse()[0]);
		setHeight(ZELLE*fenster.getMinoVerwalter().getRaetselgroesse()[1]);
		zeichnen(true);
	}
	
	/** veranlasst das Zeichnen des Feldes mit oder ohne Farbe */
	public void zeichnen(boolean farbe){
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, getWidth(), getHeight());
		
		if ( farbe ){
			block();
			raster();
			zahlen();
		} else {
			raster();
			zahlen();
		}

	}
	
	/** aktualisiert die neue Canvasgroesse nachdem ein Spielstand geladen wurde */
	void updateCanvasgroesse(){
		setWidth(ZELLE*fenster.getMinoVerwalter().getRaetselgroesse()[0]);
		setHeight(ZELLE*fenster.getMinoVerwalter().getRaetselgroesse()[1]);
	}
	
	/** faerbt die Felder */
	private void block(){
		for(int x = 0; x < fenster.getMinoVerwalter().getMinoFeld().length; x++){
			for(int y = 0; y < fenster.getMinoVerwalter().getMinoFeld()[x].length; y++){
				if ( fenster.getMinoVerwalter().getMinoFeld()[x][y] != null ){
					if (fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 1){
						gc.setFill(Color.LIGHTCORAL);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 2){
						gc.setFill(Color.CHARTREUSE);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 3){
						gc.setFill(Color.CORNFLOWERBLUE);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 4){
						gc.setFill(Color.YELLOW);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 5){
						gc.setFill(Color.MAGENTA);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 6){
						gc.setFill(Color.AQUA);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 7){
						gc.setFill(Color.CRIMSON);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 8){
						gc.setFill(Color.ORANGE);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 9){
						gc.setFill(Color.DARKGOLDENROD);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 10){
						gc.setFill(Color.GRAY);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 11){
						gc.setFill(Color.GREEN);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() == 12){
						gc.setFill(Color.RED);
						gc.fillRect(fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0]*ZELLE, fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					}
				}
			}
		}
	}
	
	/** zeichnet das Gitter */
	private void raster(){
		gc.setStroke(Color.BLACK);
		for(int x = 0; x < fenster.getMinoVerwalter().getRaetselgroesse()[0]; x++){
			for(int y = 0; y < fenster.getMinoVerwalter().getRaetselgroesse()[1]; y++){
				gc.strokeRect(x*ZELLE, y*ZELLE, ZELLE, ZELLE);
			}
		}
	}
	
	/** zeichnet die Zahlen in den Feldern */
	private void zahlen(){
		for(int x = 0; x < fenster.getMinoVerwalter().getMinoFeld().length; x++){
			for(int y = 0; y < fenster.getMinoVerwalter().getMinoFeld()[x].length; y++){
				if ( fenster.getMinoVerwalter().getMinoFeld()[x][y] != null & fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() >= 1 & fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert() <= 10 ){
					gc.strokeText(""+fenster.getMinoVerwalter().getMinoFeld()[x][y].getWert(), ZELLE*fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[0] + 10, ZELLE*fenster.getMinoVerwalter().getMinoFeld()[x][y].getPunkt()[1] + 20);
				}
			}
		}
	}
	
	/**
	 * nimmt die Zelle entgegen die berechnet wurde und setzt den Wert
	 * in der Klasse Fenster
	 * @param x zelle X
	 * @param y zelle Y
	 */
	private void analysiereKlick(int x, int y, boolean p){
		fenster.getMinoVerwalter().verarbeite(x,y,p);
	}

	/** ueberschriebene Methode aus EventHandler */
	@Override
	public void handle(MouseEvent me) {
		int px = (int) me.getX();
		int py = (int) me.getY();
		int zx = px/ZELLE;
		int zy = py/ZELLE;
		
		if ( me.getButton() == MouseButton.PRIMARY){
			analysiereKlick(zx,zy,true);
		} else if ( me.getButton() == MouseButton.SECONDARY){
			analysiereKlick(zx,zy,false);
		}
	}

}

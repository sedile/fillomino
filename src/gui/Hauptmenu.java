package gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/** Dient zum Erzeugen der Feldgroesse */
public class Hauptmenu extends Stage implements EventHandler<ActionEvent>{
	
	private BorderPane layout;
	private Label text, textB, textH;
	private TextField eingabeB, eingabeH;
	private Button ok;
	
	/** Konstruktor erzeugt das Fensteraussehen */
	Hauptmenu(){
		initKomponenten();
		initLayout();
		
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setResizable(false);
		setTitle("Start");
		show();
	}
	
	/** initialisiert die Komponenten */
	private void initKomponenten(){
		text = new Label("Groesse des Feldes eingeben");
		textB = new Label("Breite");
		textH = new Label("Hoehe");
		eingabeB = new TextField();
		eingabeH = new TextField();
		ok = new Button("Los!");
		ok.setOnAction(this);
	}
	
	/** initialisiert das Layout */
	private void initLayout(){
		layout = new BorderPane();
		layout.setTop(text);
	
		GridPane m = new GridPane();
		m.add(textB, 0, 0);
		m.add(eingabeB, 1, 0);
		m.add(textH, 0, 1);
		m.add(eingabeH, 1, 1);
		layout.setCenter(m);
		
		layout.setBottom(ok);
	}
	
	/** prueft die Benutzereingaben und gibt im Falle einer falschen
	 * Eingabe, eine Fehlermeldung aus */
	private void pruefeEingabe(){
		try {
			int b = Integer.parseInt(eingabeB.getText());
			int h = Integer.parseInt(eingabeH.getText());
			if ( b < 6 | b > 25 | h < 6 | h > 25){
				eingabeB.setText("6");
				eingabeH.setText("6");
				Alert hinweis = new Alert(AlertType.INFORMATION);
				hinweis.setHeaderText("Fehlerhafte Eingabe");
				hinweis.setContentText("Es duerfen nur Werte zwischen 6 und 25 eingegeben werden.");
				hinweis.showAndWait();
			} else {
				new Fenster(b,h);
				close();
			}
		} catch ( NumberFormatException num ){
			eingabeB.setText("6");
			eingabeH.setText("6");
			Alert fehler = new Alert(AlertType.ERROR);
			fehler.setHeaderText("Fehlerhafte Eingabe");
			fehler.setContentText("Bitte nur Zahlen eingeben");
			fehler.showAndWait();
		}
	}

	/** ueberschriebene Methode aus EventHandler */
	@Override
	public void handle(ActionEvent action) {
		if ( action.getSource() == ok ){
			pruefeEingabe();
		}
		
	}

}

package gui;

import element.Mino;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

/** Dieses Fenster zeigt die Loesung des Raetsel an */
public class LFenster extends Stage {
	
	private final int ZELLE = 32;
	
	/**
	 * erzeugt ein neues Fenster mit der Raetselloesung
	 * @param loesung die Loesung zum zeichnen
	 */
	LFenster(Mino[][] loesung){
		initModality(Modality.APPLICATION_MODAL);
		Canvas leinwand = new Canvas();
		leinwand.setWidth(ZELLE*loesung.length);
		leinwand.setHeight(ZELLE*loesung[0].length);
		GraphicsContext gc = leinwand.getGraphicsContext2D();
		zeichnen(gc,loesung);
		BorderPane layout = new BorderPane(leinwand);
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setTitle("Fillomino : Loesung");
		setResizable(false);
		show();
	}
	
	/** zeichnet die Loesung
	 * 
	 * @param gc Graphics Context
	 * @param loesung loesung
	 */
	private void zeichnen(GraphicsContext gc,Mino[][] loesung){
		block(gc,loesung);
		raster(gc,loesung);
		zahlen(gc,loesung);

	}
	
	/** faerbt die Felder
	 * 
	 * @param gc Graphics Context
	 * @param loesung loesung
	 */
	private void block(GraphicsContext gc,Mino[][] loesung){
		for(int x = 0; x < loesung.length; x++){
			for(int y = 0; y < loesung[x].length; y++){
				if ( loesung[x][y] != null ){
					if (loesung[x][y].getWert() == -1){
						gc.setFill(Color.BLACK);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if (loesung[x][y].getWert() == 1){
						gc.setFill(Color.LIGHTCORAL);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( loesung[x][y].getWert() == 2){
						gc.setFill(Color.CHARTREUSE);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( loesung[x][y].getWert() == 3){
						gc.setFill(Color.CORNFLOWERBLUE);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( loesung[x][y].getWert() == 4){
						gc.setFill(Color.YELLOW);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( loesung[x][y].getWert() == 5){
						gc.setFill(Color.MAGENTA);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( loesung[x][y].getWert() == 6){
						gc.setFill(Color.AQUA);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( loesung[x][y].getWert() == 7){
						gc.setFill(Color.CRIMSON);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					} else if ( loesung[x][y].getWert() == 8){
						gc.setFill(Color.ORANGE);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( loesung[x][y].getWert() == 9){
						gc.setFill(Color.DARKGOLDENROD);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);		
					} else if ( loesung[x][y].getWert() == 10){
						gc.setFill(Color.GRAY);
						gc.fillRect(loesung[x][y].getPunkt()[0]*ZELLE, loesung[x][y].getPunkt()[1]*ZELLE, ZELLE, ZELLE);	
					}
				}
			}
		}
	}
	
	/** zeichnet das Gitter
	 * 
	 * @param gc Graphics Context
	 * @param loesung loesung
	 */
	private void raster(GraphicsContext gc,Mino[][] loesung){
		gc.setStroke(Color.BLACK);
		for(int x = 0; x < loesung.length; x++){
			for(int y = 0; y < loesung[x].length; y++){
				gc.strokeRect(x*ZELLE, y*ZELLE, ZELLE, ZELLE);
			}
		}
	}
	
	/** zeichnet die Zahlen in den Feldern
	 * 
	 * @param gc Graphics Context
	 * @param loesung loesung
	 */
	private void zahlen(GraphicsContext gc,Mino[][] loesung){
		for(int x = 0; x < loesung.length; x++){
			for(int y = 0; y < loesung[x].length; y++){
				if ( loesung[x][y] != null ){
					gc.strokeText(""+loesung[x][y].getWert(), ZELLE*loesung[x][y].getPunkt()[0] + 10, ZELLE*loesung[x][y].getPunkt()[1] + 20);
				}
			}
		}
	}

}

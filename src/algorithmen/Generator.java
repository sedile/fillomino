package algorithmen;

import java.util.ArrayList;

import element.Mino;
import element.MinoBlock;
import element.Punkt;

/**
 * Der Generator generiert ein Fillomino-Raetsel, indem es zuerst eine
 * Feldgroesse entgegennimmt und dann Minogruppen erzeugt
 */
public class Generator {
	
	private ArrayList<MinoBlock> block;
	private Mino[][] feld;
	private final int BREITE, HOEHE;
	private final int MAXREKURSION = 10;
	private int rek;
	private boolean genFehler = false;
	
	/**
	 * Der Generator erzeugt ein Fillomino-Raetsel mit einer bestimmten
	 * Spielfeldgroesse
	 * @param b breite
	 * @param h hoehe
	 */
	public Generator(int b, int h){
		BREITE = b;
		HOEHE = h;
		feld = new Mino[BREITE][HOEHE];
		block = new ArrayList<MinoBlock>();
		
		generieren();
		int schleife = MAXREKURSION;
		while ( schleife > 1){
			fuellen(schleife);
			schleife--;
		}
		einsFueller();
		nulltester();
	}
	
	/**
	 * durchsucht das Feld nach Nullfeldern und setzt den 'bestaetigungswert' auf
	 * true, falls ein 0-Feld gefunden wurde (Fehler)
	 */
	private void nulltester(){
		for(int x = 0; x < BREITE; x++){
			for(int y = 0; y < HOEHE; y++){
				if ( feld[x][y].getWert() == 0){
					genFehler = true;
					return;
				}
			}
		}
	}

	/**
	 * durchlauft alle Felder und sucht Positionen die 0 sind (freie Felder),
	 * und versucht diese zu fuellen.
	 * @param schleife Anzahl der Durchlaeufe und der Wert zu dem Versucht wird
	 * eine Minogruppe zu bilden
	 */
	private void fuellen(int schleife){
		for(int x = 0; x < BREITE; x++){
			for(int y = 0; y < HOEHE; y++){
				if ( feld[x][y].getWert() == 0){
					position(new Punkt(x,y), schleife);
				}
			}
		}
	}
	
	/**
	 * fuellt die isolierten Felder mit 1 auf. Alle restlichen Felder
	 * sind nicht besetzt (0).
	 */
	private void einsFueller(){
		for(int x = 0; x < BREITE; x++){
			for(int y = 0; y < HOEHE; y++){
				if ( feld[x][y].getWert() == 0){
					int w = 0;
					if ( pruefeGrenzen(new Punkt(x,y-1)) ){
						if ( feld[x][y-1].getWert() > 1){
							w++;
						}
					} else {
						w++;
					}
					if ( pruefeGrenzen(new Punkt(x+1,y)) ){
						if ( feld[x+1][y].getWert() > 1){
							w++;
						}
					} else {
						w++;
					}
					if ( pruefeGrenzen(new Punkt(x,y+1)) ){
						if ( feld[x][y+1].getWert() > 1){
							w++;
						}
					} else {
						w++;
					}
					if ( pruefeGrenzen(new Punkt(x-1,y)) ){
						if ( feld[x-1][y].getWert() > 1){
							w++;
						}
					} else {
						w++;
					}
					if ( w == 4){
						feld[x][y].setWert(1);
						block.add(new MinoBlock());
						block.get(block.size() - 1).addMino(new Punkt(x,y), 1);
					}
				}
			}
		}
	}

	/** durchlaeuft alle moeglichen Positionen, die leer (0) sind */
	private void generieren(){
		for(int x = 0; x < BREITE; x++){
			for(int y = 0; y < HOEHE; y++){
				feld[x][y] = new Mino(new Punkt(x,y),0);
			}
		}

		for(int x = 0; x < BREITE; x++){
			for(int y = 0; y < HOEHE; y++){
				if ( feld[x][y].getWert() == 0){
					position(new Punkt(x,y), zufallswert());
				}
			}
		}
	}
	
	/**
	 * versucht Minos als Gruppe in das Spielfeld einzufuegen
	 * @param p Die Koordinate
	 * @param wert der Gruppenwert der gesetzt werden soll
	 */
	private void position(Punkt p, int wert){
		if ( !istGueltigePosition(p) ){
			return;
		} else {
			ArrayList<Punkt> positionen = new ArrayList<Punkt>();
			positionen.add(p);
			positionen = nachbarsuche(positionen, wert);
			if ( positionen.size() == 0){
				position(p, zufallswert());
			}
			einfuegen(positionen,wert);
		}
	}
	
	/**
	 * erweitert die Liste aller bereits erfassten Positionen und fuegt weiter hinzu,
	 * solange es noch solche Positionen gibt. Es wird solange gesucht bis der wert
	 * ausgeschoepft, oder ein Nachbarkonflikt entsteht.
	 * @param positionen Liste aller gueltigen Positionen
	 * @param wert der Minowert
	 */
	private void einfuegen(ArrayList<Punkt> positionen, int wert){
		/* Ermittelte Minos reichen aus */
		if ( wert <= positionen.size() ){
			block.add(new MinoBlock());
			int idx = block.size() - 1;
			for(int n = 0; n < positionen.size(); n++){
				feld[positionen.get(n).getX()][positionen.get(n).getY()].setWert(wert);
				block.get(idx).addMino(new Punkt(positionen.get(n).getX(),positionen.get(n).getY()), wert);
			}
		}
	}
	
	/**
	 * entfernt Duplikatpositionen aus der Liste
	 * @param dupl Liste mit moeglichen Duplikaten
	 * @return Liste ohne Duplikate
	 */
	private ArrayList<Punkt> entferneDuplikate(ArrayList<Punkt> dupl ){
		for(int a = 0; a < dupl.size() - 1; a++){
			for(int b = 1 + a; b < dupl.size(); b++){
				if ( dupl.get(a).getX() == dupl.get(b).getX() & dupl.get(a).getY() == dupl.get(b).getY() ){
					dupl.remove(a);
				}
			}
		}
		return dupl;
	}

	/**
	 * prueft die Nachbarpositionen und fuegt die Koordinaten in eine Liste ein, wenn
	 * die Minogruppe noch nicht ausreicht ( size ist kleiner als wert )
	 * @param pos alle gueltigen Koordinaten
	 * @param wert der Minowert
	 * @return Liste mit neu gefundenen Nachbarn, sofern existiert
	 */
	private ArrayList<Punkt> nachbarsuche(ArrayList<Punkt> pos, int wert){
		if ( pos.size() == wert){
			return pos;
		}

		ArrayList<Punkt> nachbarn = new ArrayList<Punkt>();
		for(int n = 0; n < pos.size(); n++){
			Punkt p = pos.get(n);
			nachbarn = nachbar(p, pos.size(),wert);
		}		

		pos.addAll(nachbarn);
		pos = entferneDuplikate(pos);
		for(int t = 0; t < pos.size(); t++){
			boolean n = pruefeNachbarFeld(pos.get(t).getX(), pos.get(t).getY()-1, wert);
			boolean o = pruefeNachbarFeld(pos.get(t).getX()+1, pos.get(t).getY(), wert);
			boolean s = pruefeNachbarFeld(pos.get(t).getX(), pos.get(t).getY()+1, wert);
			boolean w = pruefeNachbarFeld(pos.get(t).getX()-1, pos.get(t).getY(), wert);
			
			if ( !n | !o | !s | !w ){
				pos.clear();
				return pos;
			}
		}
		
		if (pos.size() < wert & rek < MAXREKURSION){
			rek++;
			nachbarsuche(pos,wert);
		}
		
		rek = 0;
		return pos;
	}

	/**
	 * findet zu einer gegebenen Position, moegliche Nachbarn und gibt Sie
	 * als eine Liste zurueck
	 * @param p uebergebener Punkt, zu dem Nachbarn gesucht werden
	 * @param minos maximale Anzahl an Minos (Abbruchbedingung)
	 * @param wert Der maximale Minowert den es zu erreichen gilt
	 * @return Liste aller neuen Punkte
	 */
	private ArrayList<Punkt> nachbar(Punkt p, int minos, int wert){
		ArrayList<Punkt> nachbarn = new ArrayList<Punkt>();
		if ( !konflikt(new Punkt(p.getX(), p.getY()-1), wert)){
			nachbarn.add(new Punkt(p.getX(), p.getY()-1));
			minos++;
			if ( minos == wert){
				return nachbarn;
			}
		}
		if ( !konflikt(new Punkt(p.getX()+1, p.getY()), wert)){
			nachbarn.add(new Punkt(p.getX()+1, p.getY()));
			minos++;
			if ( minos == wert){
				return nachbarn;
			}
		}
		if ( !konflikt(new Punkt(p.getX(), p.getY()+1), wert)){
			nachbarn.add(new Punkt(p.getX(), p.getY()+1));
			minos++;
			if ( minos == wert){
				return nachbarn;
			}
		}
		if ( !konflikt(new Punkt(p.getX()-1, p.getY()), wert)){
			nachbarn.add(new Punkt(p.getX()-1, p.getY()));
			minos++;
			if ( minos == wert){
				return nachbarn;
			}
		}
		return nachbarn;
	}
	
	/**
	 * prueft, ob die uebergebene Koordinate gueltig ist. D.h.
	 * diese Position kann fuer ein Mino/Minogruppe benutzt werden.
	 * @param p zu pruefende Koordinate
	 * @param wert der gruppenwert
	 * @return true, falls konflikt besteht
	 */
	private boolean konflikt(Punkt p, int wert){
		if ( !pruefeGrenzen(p) ){
			return true;
		}
		if (feld[p.getX()][p.getY()].getWert() > 0){
			return true;
		}
		return false;
	}

	/**
	 * prueft, ob die uebergebene Koordinate gueltig ist. D.h.
	 * es werden Grenzen und Feldwert geprueft
	 * @param p zu pruefende Koordinate
	 * @return true, gueltige Position, sonst false
	 */
	private boolean istGueltigePosition(Punkt p){
		if ( pruefeGrenzen(p) & pruefeFeld(p) ){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * prueft, ob die Feldposition leer ist
	 * @param p Koordinate
	 * @return true, kann benutzt werden, sonst false
	 */
	private boolean pruefeFeld(Punkt p){
		if ( feld[p.getX()][p.getY()].getWert() == 0){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * prueft, ob eine bestimmte Position schon ein Wert vordefiniert hat.
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 * @param wert zu vergleichender Wert
	 * @return true, alles ok, false, konflikt
	 */
	private boolean pruefeNachbarFeld(int x, int y, int wert){
		if ( pruefeGrenzen(new Punkt(x,y)) ){
			if ( feld[x][y].getWert() == wert){
				return false;
			}
		}
		return true;
	}

	/**
	 * prueft, ob die Feldgrenzen eingehalten werden
	 * @param p zu pruefende Koordinate
	 * @return true, Grenzen sind ok, sonst false
	 */
	private boolean pruefeGrenzen(Punkt p){
		if ( p.getX() < 0 | p.getX() >= BREITE | p.getY() < 0 | p.getY() >= HOEHE){
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * generiert ein Zufallswert
	 * @return zufallszahl
	 */
	private int zufallswert(){
		return (int) (Math.random() * 9) + 2;
	}
	
	/**
	 * gibt true zurueck, wenn ein Fillominoraetsel fehlerfrei generiert wurde
	 * sonst false
	 * @return boolescher Wert der Pruefung
	 */
	public boolean getOK(){
		return genFehler;
	}
	
	/** gibt das fertig generierte Raetsel zurueck
	 * @return das Raetsel
	 */
	public Mino[][] getLoesung(){
		return feld;
	}
	
	/**
	 * gibt eine ungeloeste Variante des Raetsels, wobei dem
	 * Spieler einige Felder aufgedeckt werden
	 * @return ungeloestes Raetsel
	 */
	public Mino[][] getRaetsel(){
		
		/* Initialisiere die Feldelemente */
		Mino[][] raetsel = new Mino[BREITE][HOEHE];
		for(int e = 0; e < BREITE; e++){
			for(int f = 0; f < HOEHE; f++){
				raetsel[e][f] = new Mino(new Punkt(e,f), 0);
			}
		}
		
		/* ein paar Felder werden vorgegeben */
		for(int x = 0; x < block.size(); x++){
			Mino ms = block.get(x).getMinogruppe().get(0);
			Mino ns = block.get(x).getLetzter();
			
			/* lasse jeden achten Block leer */
			if ( x % 8 == 0 & ms.getWert() > 1){
				continue;
			}
			
			if ( x % 2 == 0){
				if ( ms.getWert() <= 6){
					/* Ein Element wird vorgegeben */
					raetsel[ms.getPunkt()[0]][ms.getPunkt()[1]].setWert(ms.getWert());
					raetsel[ms.getPunkt()[0]][ms.getPunkt()[1]].setStarter(true);
				} else if ( ms.getWert() > 6 & ms.getWert() <= 10){
					/* Zwei Elemente werden vorgegeben */
					raetsel[ms.getPunkt()[0]][ms.getPunkt()[1]].setWert(ms.getWert());
					raetsel[ms.getPunkt()[0]][ms.getPunkt()[1]].setStarter(true);
					raetsel[ns.getPunkt()[0]][ns.getPunkt()[1]].setWert(ns.getWert());
					raetsel[ns.getPunkt()[0]][ns.getPunkt()[1]].setStarter(true);
				}
			} else {
				raetsel[ms.getPunkt()[0]][ms.getPunkt()[1]].setWert(ms.getWert());
				raetsel[ms.getPunkt()[0]][ms.getPunkt()[1]].setStarter(true);
			}
		}
		return raetsel;
	}
}

package manager;

import java.util.StringTokenizer;

public class Parser {

	private Symbol aktSymbol;
	private String aktToken;
	private StringTokenizer tokenizer;
	private boolean syntaxKorrekt;
	
	Parser(String text){
		syntaxKorrekt = false;
		aktToken = text;
		tokenizer = new StringTokenizer(aktToken);
		syntaxKorrekt = parseText();
	}
	
	private boolean parseText(){
		aktSymbol = getNaechstesSymbol();
		if ( !feld() ){
			return false;
		}
		if ( !aktuell() ){
			return false;
		}
		if ( !loesung() ){
			return false;
		}		
		return true;
	}

	private boolean feld(){
		if ( aktSymbol == Symbol.FELD){
			aktSymbol = getNaechstesSymbol();
			if ( !feldDaten() ){
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	private boolean aktuell(){
		if ( aktSymbol == Symbol.AKT){
			aktSymbol = getNaechstesSymbol();
			
			while ( aktSymbol == Symbol.LKLAMMER ){
				if ( !zeile() ){
					return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}
	
	private boolean loesung(){
		if ( aktSymbol == Symbol.LSG){
			aktSymbol = getNaechstesSymbol();
			
			while ( aktSymbol == Symbol.LKLAMMER ){
				if ( !zeile() ){
					return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}
	
	private boolean feldDaten(){
		if ( !ziffer() ){
			return false;
		}
		
		if ( aktSymbol == Symbol.X ){
			aktSymbol = getNaechstesSymbol();
			if ( !ziffer() ){
				return false;
			}
		} else {
			return false;
		}
		return true;
	}
	
	/**
	 * prueft die syntaktische Korrektheit einer Zeile
	 * @return true, kein Syntaxfehler
	 */
	private boolean zeile(){
		if ( aktSymbol == Symbol.LKLAMMER){
			aktSymbol = getNaechstesSymbol();
			
			if ( !ziffer() ){
				return false;
			}
			
			if ( aktSymbol == Symbol.KOMMA ){
				aktSymbol = getNaechstesSymbol();
				
				if ( !ziffer() ){
					return false;
				}
				
				if ( aktSymbol == Symbol.KOMMA ){
					aktSymbol = getNaechstesSymbol();
					
					if ( !ziffer() ){
						return false;
					}
					
					if ( aktSymbol == Symbol.STRICH ){
						aktSymbol = getNaechstesSymbol();
						
						if ( aktSymbol == Symbol.TRUE | aktSymbol == Symbol.FALSE ){
							aktSymbol = getNaechstesSymbol();
							
							if ( aktSymbol == Symbol.RKLAMMER ){
								aktSymbol = getNaechstesSymbol();
							} else {
								return false;
							}							
						} else {
							return false;
						}
					} else {
						return false;
					}
				} else {
					return false;
				}				
			} else {
				return false;
			}			
		} else {
			return false;
		}
		return true;
	}
	
	/**
	 * prueft, ob eine Ziffer als Symbol folgt
	 * @return true, eine Zahl
	 */
	private boolean ziffer(){
		if ( aktSymbol == Symbol.ZAHL ){
			aktSymbol = getNaechstesSymbol();
			return true;
		}
		return false;
	}
	
	/**
	 * gibt das aktuelle Symbol zurueck, falls es sich um ein gueltiges Symbol
	 * handelt, sonst DIV
	 * @return Symbol
	 */
	private Symbol getNaechstesSymbol(){
		if ( tokenizer.hasMoreTokens() ){
			aktToken = tokenizer.nextToken();
			
			switch(aktToken){
			case "#FELD"	:	return Symbol.FELD;
			case "#AKT"		:	return Symbol.AKT;
			case "#LSG"		:	return Symbol.LSG;
			case "X"		:	return Symbol.X;
			case "<"		:	return Symbol.LKLAMMER;
			case ">"		:	return Symbol.RKLAMMER;
			case "true"		:	return Symbol.TRUE;
			case "false"	:	return Symbol.FALSE;
			case ","		:	return Symbol.KOMMA;
			case "|"		:	return Symbol.STRICH;
			
			case "-1"		:	return Symbol.ZAHL;
			case "0"		:	return Symbol.ZAHL;
			case "1"		:	return Symbol.ZAHL;
			case "2"		:	return Symbol.ZAHL;
			case "3"		:	return Symbol.ZAHL;
			case "4"		:	return Symbol.ZAHL;
			case "5"		:	return Symbol.ZAHL;
			case "6"		:	return Symbol.ZAHL;
			case "7"		:	return Symbol.ZAHL;
			case "8"		:	return Symbol.ZAHL;
			case "9"		:	return Symbol.ZAHL;
			case "10"		:	return Symbol.ZAHL;
			case "11"		:	return Symbol.ZAHL;
			case "12"		:	return Symbol.ZAHL;
			case "13"		:	return Symbol.ZAHL;
			case "14"		:	return Symbol.ZAHL;
			case "15"		:	return Symbol.ZAHL;
			case "16"		:	return Symbol.ZAHL;
			case "17"		:	return Symbol.ZAHL;
			case "18"		:	return Symbol.ZAHL;
			case "19"		:	return Symbol.ZAHL;
			case "20"		:	return Symbol.ZAHL;
			case "21"		:	return Symbol.ZAHL;
			case "22"		:	return Symbol.ZAHL;
			case "23"		:	return Symbol.ZAHL;
			case "24"		:	return Symbol.ZAHL;
			case "25"		:	return Symbol.ZAHL;
			}
		}
		return Symbol.DIV;
	}
	
	/**
	 * gibt zurueck, ob ein Syntaxfehler entdeckt wurde
	 * @return true, ein Syntaxfehler
	 */
	public boolean getParserErgebnis(){
		return syntaxKorrekt;
	}

}

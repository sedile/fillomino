package manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import element.Mino;
import element.Punkt;
import gui.Fenster;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/** Diese Klasse ist fuer das Laden eines Spielstandes da */
public class Laden {
	
	private Fenster fenster;
	private int[] raetselgroesse;
	private Mino[][] aktuell, loesung;
	private FileChooser fc;
	private String zeile = "";
	private String eingelesenerText = new String();	
	
	/**
	 * Erzeugt ein FileChooser
	 * @param f Fensterreferenz
	 */
	public Laden(Fenster f){
		fenster = f;
		fc = new FileChooser();
		fc.setTitle("Spielstand laden");
		fc.getExtensionFilters().addAll(new ExtensionFilter("FRD-Datei", "*.frd"));	
		spielLaden();
		if ( pruefeText() ){
			uebertrageDaten();	
		}
	}
	
	/** steuert das Einladen einer Datei */
	private void spielLaden() {
		File datei = fc.showOpenDialog(fenster);
		
		if ( datei != null ){
			try {		
				BufferedReader br = new BufferedReader(new FileReader(datei.getPath()));
						
				while ((zeile = br.readLine()) != null ){
					eingelesenerText += zeile+" ";
				}
				
				if ( br != null){
					br.close();
				}
			} catch ( Exception e){
				Alert f = new Alert(AlertType.ERROR);
				f.setHeaderText("Dateifehler (Einlesefehler)");
				f.setContentText("Fehler beim einlesen des Spielstandes");
				f.showAndWait();
			}
		} else {
			return;
		}
	}
	
	/** prueft die Syntaktische Korrektheit der eingeladenen Datei
	 * 
	 * @return true, wenn kein Syntaxfehler aufgetreten ist, sonst false
	 */
	private boolean pruefeText(){
		Parser parser = new Parser(eingelesenerText);
		if ( !parser.getParserErgebnis() ){
			Alert f = new Alert(AlertType.ERROR);
			f.setHeaderText("Dateifehler (Syntaxfehler, Parser)");
			f.setContentText("Das Dateiformat ist beschaedigt oder enthÃ¤lt nicht erlaubte Zeichen");
			f.showAndWait();
			return false;
		}
		return true;
	}
	
	/** wandelt die Datei passend um, um weiter fortzufahren */
	private void uebertrageDaten(){
		eingelesenerText = eingelesenerText.replace("#FELD","");
		eingelesenerText = eingelesenerText.replace("#AKT","");
		eingelesenerText = eingelesenerText.replace("#LSG","");
		eingelesenerText = eingelesenerText.replace("<"," ");
		eingelesenerText = eingelesenerText.replace(">"," ");
		eingelesenerText = eingelesenerText.replace(","," ");
		eingelesenerText = eingelesenerText.replace("|"," ");
		eingelesenerText = eingelesenerText.replace("X"," ");
		eingelesenerText = eingelesenerText.replace("true","-2");
		eingelesenerText = eingelesenerText.replace("false","-3");
		
		StringTokenizer token = new StringTokenizer(eingelesenerText);
		final int TOKENLAENGE = token.countTokens();
		int[] zahlen = new int[token.countTokens()];
		
		/* Wandel die Strings in Zahlen um und speichere Sie in das Array */
		try {
			for(int i = 0; i < TOKENLAENGE; i++){
				zahlen[i] = Integer.parseInt(token.nextToken());
			}
		} catch ( NumberFormatException num ){
			Alert f = new Alert(AlertType.ERROR);
			f.setHeaderText("Dateifehler (Syntaxfehler, Format)");
			f.setContentText("nicht erlaubtes Zeichen entdeckt");
			f.showAndWait();
			return;
		}

		if ( speichereGeladeneDatenAb(zahlen) ){
			fenster.close();
			new Fenster(raetselgroesse,aktuell,loesung);
		} else {
			Alert f = new Alert(AlertType.ERROR);
			f.setHeaderText("Dateifehler (Semantikfehler)");
			f.setContentText("Das Dateiformat enthaelt ungueltige Wertbelegungen");
			f.showAndWait();
		}
	}
	
	/**
	 * laedt die einzelnen Werte in Datenstrukturen ein und preuft, ob die Werte
	 * korrekt sind
	 * @param zahlen zahlenfeld
	 * @return true, kein Semantikfehler
	 */
	private boolean speichereGeladeneDatenAb(int[] zahlen){
		int idx = 0;
		if ( zahlen[0] >= 6 & zahlen[0] <= 25 & zahlen[1] >= 6 & zahlen[1] <= 25){
			raetselgroesse = new int[]{zahlen[0],zahlen[1]};
		} else {
			return false;
		}
		aktuell = new Mino[zahlen[0]][zahlen[1]];
		loesung = new Mino[zahlen[0]][zahlen[1]];
		
		idx += 2;
		for(int i = idx; i < idx + 4*(zahlen[0]*zahlen[1]); i += 4){
			if ( zahlen[i] >= 0 & zahlen[i] <= 24 & zahlen[i+1] >= 0 & zahlen[i+1] <= 24 & zahlen[i+2] >= -1 & zahlen[i+2] <= 10){
				aktuell[zahlen[i]][zahlen[i+1]] = new Mino(new Punkt(zahlen[i],zahlen[i+1]),zahlen[i+2]);
				if ( zahlen[i+3] == -2 ){
					aktuell[zahlen[i]][zahlen[i+1]].setStarter(true);
				} else if ( zahlen[i+3] == -3 ){
					aktuell[zahlen[i]][zahlen[i+1]].setStarter(false);
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		
		idx += 4*(zahlen[0]*zahlen[1]);
		for(int i = idx; i < idx + 4*(zahlen[0]*zahlen[1]); i += 4){
			if ( zahlen[i] >= 0 & zahlen[i] <= 24 & zahlen[i+1] >= 0 & zahlen[i+1] <= 24 & zahlen[i+2] >= -1 & zahlen[i+2] <= 10){
				loesung[zahlen[i]][zahlen[i+1]] = new Mino(new Punkt(zahlen[i],zahlen[i+1]),zahlen[i+2]);
				if ( zahlen[i+3] == -2 ){
					loesung[zahlen[i]][zahlen[i+1]].setStarter(true);
				} else if ( zahlen[i+3] == -3 ){
					loesung[zahlen[i]][zahlen[i+1]].setStarter(false);
				} else {
					return false;
				}
			} else {
				return false;
			}
		}		
		return true;
	}

}

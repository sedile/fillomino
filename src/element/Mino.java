package element;

/** Diese Klasse erzeugt Minos, die Elemente fuer das Raetsel */
public class Mino {
	
	private Punkt punkt;
	private int wert;
	private boolean starter;
	
	/**
	 * erzeugt ein Mino, indem ihm direkt ein Punkt als Koordinate
	 * und der Gruppenwert uebergeben wird
	 * @param p Koordinate (x,y)
	 * @param wert gruppenwert
	 */
	public Mino(Punkt p, int wert){
		punkt = p;
		this.wert = wert;
		starter = false;
	}

	/**
	 * markiert ein Element einer Minogruppe als Starter, kann
	 * nicht entfernt werden.
	 * @param istStarter true, starterelement, sonst false
	 */
	public void setStarter(boolean istStarter){
		starter = istStarter;
	}
	
	/** gibt zurueck, ob es sich bei diesen Mino um ein Starterelement
	 * handelt
	 * @return true, startblock
	 */
	public boolean getStarter(){
		return starter;
	}
	
	/**
	 * veraendert die Koordinate eines Punktes
	 * @param x neue x-Koordinate
	 * @param y neue y-Koordinate
	 */
	public void setPunkt(int x, int y){
		getPunkt()[0] = x;
		getPunkt()[1] = y;
	}
	
	/**
	 * gibt die Koordinate als integerarray zurueck
	 * @return 0 = x-Koordinate, 1 = y-Koordinate
	 */
	public int[] getPunkt(){
		return new int[]{punkt.getX(), punkt.getY()};
	}
	
	/**
	 * setzt den neuen Minowert bzw. aendert ein bestehenden
	 * Wert um
	 * @param wert neuer Minowert
	 */
	public void setWert(int wert){
		this.wert = wert;
	}
	
	/**
	 * gibt den Gruppenwert zurueck
	 * @return gruppenwert
	 */
	public int getWert(){
		return wert;
	}

}

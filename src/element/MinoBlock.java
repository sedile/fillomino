package element;

import java.util.ArrayList;

/** erzeugt eine Liste, die Minos verwaltet */
public class MinoBlock {
	
	private ArrayList<Mino> gruppe;
	
	/**
	 * erzeugt eine leere Liste, die mit Minos befuellt werden kann
	 */
	public MinoBlock(){
		gruppe = new ArrayList<Mino>();
	}

	/**
	 * gibt den letzten Index der Liste zurueck
	 * @return letzter Listenindex
	 */
	public int letzter(){
		return gruppe.size() - 1;
	}
	
	/**
	 * gibt das letzte Mino aus der Liste zurueck
	 * @return letztes Mino in der Liste
	 */
	public Mino getLetzter(){
		return gruppe.get(letzter());
	}
	
	/**
	 * fuegt ein Mino in die Liste ein
	 * @param p Minokoordinate
	 * @param wert Minowert
	 */
	public void addMino(Punkt p, int wert){
		gruppe.add(new Mino(p,wert));
	}

	/**
	 * gibt die komplette Minogruppe, also die verkettung von
	 * Minos zurueck
	 * @return minogruppe
	 */
	public ArrayList<Mino> getMinogruppe(){
		return gruppe;
	}
}
